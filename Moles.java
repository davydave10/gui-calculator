public class Moles
{
    final double cuWeight = 63.546;
    final double avogadro = 6.022E23;
    
    // Calculate number of moles of Cu for a given
    // mass.
    public double convert(double grams)
    {
        return grams / cuWeight;
    }
}