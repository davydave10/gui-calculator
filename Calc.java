import acm.program.Program;
import acm.graphics.*;      // GCanvas
import javax.swing.*;       // JButton, JTextField, Jwhatever
import java.awt.event.*;    // Event
import java.awt.*;          // Font, colors

public class Calc extends Program
{
    private JTextField inputField;
    private JTextField outputField;
    private JTextField postfixField;
    
    public Calc()
    {
        start();
        setSize(500, 500);
    }
    
    public void init()
    {
        GCanvas canvas = new GCanvas();
        add(canvas);
        
        JLabel inputLabel = new JLabel("Infix");
        JLabel outputLabel = new JLabel("Result");
        JLabel postfixLabel = new JLabel("Postfix");
        
        inputField = new JTextField();
        outputField = new JTextField();
        postfixField = new JTextField();
        JButton goButton = new JButton("Go");
        //goButton.setActionCommand("Go");
        JButton clearButton = new JButton("Clear");
        JButton spaceButton = new JButton("space");
        JButton zeroButton = new JButton("0");
        JButton oneButton = new JButton("1");
        JButton twoButton = new JButton("2");
        JButton threeButton = new JButton("3");
        JButton fourButton = new JButton("4");
        JButton fiveButton = new JButton("5");
        JButton sixButton = new JButton("6");
        JButton sevenButton = new JButton("7");
        JButton eightButton = new JButton("8");
        JButton nineButton = new JButton("9");
        JButton plusButton = new JButton("+");
        JButton subtractButton = new JButton("-");
        JButton divButton = new JButton("/");
        JButton multiplyButton = new JButton("x");
        JButton openparenButton = new JButton("(");
        JButton closedparenButton = new JButton(")");
        JButton exponentButton = new JButton("^");
        JButton sinButton = new JButton("sin");
        JButton cosButton = new JButton("cos");
        JButton tanButton = new JButton("tan");
        
        
        
        Font bigText = new Font("Garamond", Font.BOLD, 24);
        inputLabel.setFont(bigText);
        outputLabel.setFont(bigText);
        postfixLabel.setFont(bigText);
        
        canvas.setBackground(Color.CYAN);
        
        inputField.setSize(150, 20);
        outputField.setSize(150, 20);
        postfixField.setSize(150,20);
        
        
        canvas.add(inputLabel, 50, 50);
        canvas.add(outputLabel, 50, 100);
        canvas.add(inputField, 150, 50);
        canvas.add(outputField, 150, 100);
        canvas.add(goButton, 25, 150);
        canvas.add(clearButton, 175, 150);
        canvas.add(spaceButton, 100, 150);
        canvas.add(zeroButton, 25, 300);
        canvas.add(oneButton, 25, 275);
        canvas.add(twoButton, 100, 275);
        canvas.add(threeButton, 175, 275);
        canvas.add(fourButton, 25, 250);
        canvas.add(fiveButton, 100, 250);
        canvas.add(sixButton, 175, 250);
        canvas.add(sevenButton, 25, 225);
        canvas.add(eightButton, 100, 225);
        canvas.add(nineButton, 175, 225);
        canvas.add(plusButton, 100, 200);
        canvas.add(subtractButton, 25, 200);
        canvas.add(divButton, 175, 200);
        canvas.add(multiplyButton, 25, 175);
        canvas.add(closedparenButton, 175, 175);
        canvas.add(exponentButton, 250, 175);
        canvas.add(openparenButton, 100, 175);
        
        canvas.add(sinButton, 250, 250);
        canvas.add(cosButton, 250, 225);
        canvas.add(tanButton, 250, 200);
        
        
        canvas.add(postfixField, 150, 75);
        canvas.add(postfixLabel, 50, 75);
        
        
        
        
        //ImageIcon goImage = new ImageIcon("GoButton.png");
        //goButton.setIcon(goImage);
        //goButton.setSize(goImage.getIconWidth(), goImage.getIconHeight());
        //goButton.setBorderPainted(false);
        
        //GImage background = new GImage("space.jpg");
        //canvas.add(background, 0, 0);
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent ae)
    {
        String whichButton = ae.getActionCommand();
        
        switch (whichButton)
        {
            case "Go":
            Postfix p = new Postfix();
            SY s= new SY();
            String postfix = s.convert(inputField.getText()).toString();
          
            
            postfixField.setText(postfix);
            double res = p.eval(s.convert(inputField.getText()));
            outputField.setText(""+res);
            
            break;
            
            /*
            case "Go":
            Moles m = new Moles();
            double res = m.convert(Double.parseDouble(inputField.getText()));
            outputField.setText("" + res);
            break;*/
            
            case "Clear":
            inputField.setText("");
            outputField.setText("");
            break;
            
            case "0":
            inputField.setText(inputField.getText()+"0");
            break;
            
            case "1":
            inputField.setText(inputField.getText()+"1");
            break;
            
            case "2":
            inputField.setText(inputField.getText()+"2");
            break;
            
            case "3":
            inputField.setText(inputField.getText()+"3");
            break;
            
            case "4":
            inputField.setText(inputField.getText()+"4");
            break;
            
            case "5":
            inputField.setText(inputField.getText()+"5");
            break;
            
            case "6":
            inputField.setText(inputField.getText()+"6");
            break;
            
            case "7":
            inputField.setText(inputField.getText()+"7");
            break;
            
            case "8":
            inputField.setText(inputField.getText()+"8");
            break;
            
            case "9":
            inputField.setText(inputField.getText()+"9");
            break;
            
            case "space":
            inputField.setText(inputField.getText()+" ");
            break;
            
            case "+":
            inputField.setText(inputField.getText()+"+");
            break;
            
            case "-":
            inputField.setText(inputField.getText()+"-");
            break;
            
            case "/":
            inputField.setText(inputField.getText()+"/");
            break;
            
            case "x":
            inputField.setText(inputField.getText()+"x");
            break;
            
            case "(":
            inputField.setText(inputField.getText()+"(");
            break;
            
            case ")":
            inputField.setText(inputField.getText()+")");
            break;
            
            case "^":
            inputField.setText(inputField.getText()+"^");
            break;
            
            case "sin":
            inputField.setText(inputField.getText()+"sin");
            break;
            
            case "cos":
            inputField.setText(inputField.getText()+"cos");
            break;
            
            case "tan":
            inputField.setText(inputField.getText()+"tan");
            break;
        }
    }
}