//stack.peek
import java.util.ArrayList;

public class SY
{
    public ArrayList<String> convert(String expr)
    {
        String[] tokens = expr.split(" +");
        ArrayList<String> result = new ArrayList<String>();
        Stack<Stack<String>> sofs = new Stack<Stack<String>>();
        sofs.push(new Stack<String>());

        for (String token : tokens)
        {
            switch(prec(token))
            {
                case -1:
                result.add(token);
                break;

                case -3:
                sofs.push(new Stack<String>());
                break;

                case -2:
                while (!sofs.peek().isEmpty()) result.add(sofs.peek().pop());
                sofs.pop();
                break;

                default:
                int a= prec(token);
                while(!sofs.peek().isEmpty() && prec(sofs.peek().peek()) >= a && a!= 3 && prec(sofs.peek().
                peek())!=a)
                    result.add(sofs.peek().pop());
                sofs.peek().push(token);
                break;
            }
        }

        while (!sofs.isEmpty())
        {
            while (!sofs.peek().isEmpty())result.add(sofs.peek().pop());
            sofs.pop();
        }
        return result;

    }
    


    private int prec(String op)
    {
        switch(op)
        {
            case "+":
            case "-":
            return 1;
            
            case"/":
            case "*":
            return 2;
            
            case "^":
            return 3;
            
            case "(":
            return -3;
            
            case ")":
            return -2;
            
            default:
            return -1;
        }
    }
}